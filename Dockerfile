FROM ubuntu:focal-20210119

LABEL maintainer "Jacques Supcik <jacques.supcik@hefr.ch>"

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y \
    clang \
    clang-tools \
    cmake \
    curl \
    file \
    git \
    make \
    python3 \
    python3-pip \
    unzip

ENV VERSION="10"
ENV YEAR="2020"
ENV QUARTAL="q4"

ENV TOOLCHAIN_DIR="${VERSION}-${YEAR}${QUARTAL}"
ENV TOOLCHAIN_ARCH="x86_64"
ENV TOOLCHAIN_NAME="gcc-arm-none-eabi-${VERSION}-${YEAR}-${QUARTAL}-major"

ENV TOOLCHAIN=/opt/${TOOLCHAIN_NAME}
ENV PATH=${PATH}:${TOOLCHAIN}/bin

RUN cd /opt && curl -sL https://developer.arm.com/-/media/Files/downloads/gnu-rm/${TOOLCHAIN_DIR}/${TOOLCHAIN_NAME}-${TOOLCHAIN_ARCH}-linux.tar.bz2 | tar jxf -
RUN pip3 install chardet conan cpplint
